package com.c0d3in3.lolstats

import android.graphics.Color
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.c0d3in3.lolstats.base.*
import com.c0d3in3.lolstats.base.Utils.getChampionByID
import com.c0d3in3.lolstats.base.Utils.getChampionNameByID
import com.c0d3in3.lolstats.model.MatchModel
import io.opencensus.resource.Resource
import kotlinx.android.synthetic.main.match_item_layout.view.*
import kotlinx.coroutines.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Math.pow
import java.sql.Time
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.math.exp
import kotlin.math.pow

object DataBindingComponents {

    @JvmStatic
    @BindingAdapter("setIcon")
    fun setIcon(view: ImageView, icon: Int) {
        Glide.with(view).load("$ICONS_REF$icon.png").into(view)
    }

    @JvmStatic
    @BindingAdapter("setChampionIcon")
    fun setChampionIcon(view: ImageView, id: Long) {
        var championName = ""
        CoroutineScope(Dispatchers.Main).launch{
            withContext(Dispatchers.Default){
                championName = getChampionByID(id)
            }
            Glide.with(view).load("$CHAMPION_SQUARE_IMG$championName.png").into(view)
        }
    }

    @JvmStatic
    @BindingAdapter("setChampionScore")
    fun setChampionScore(view: TextView, score: Long) {
        val number = score.toString().length-1
        when{
            number < 3 -> view.text = "$score"
            number in 3..5 ->{
                val div = 1000.0
                val points = score/div
                view.text =  "${"%.1f".format(points)}k"
            }
            else ->{
                val div = 1000000.0
                val points = score/div
                view.text =  "${"%.1f".format(points)}m"
            }
        }
    }

    @JvmStatic
    @BindingAdapter( "setBackground", "setMatchInfo", "setParticipantId")
    fun setMatchInfo(view: LinearLayout, mainView : CardView, model: MatchModel, participantId: Int) {
        if(model.gameId != (-1).toLong()){
            setQueue(view.matchTypeTextView, model.queueId)
            setGameCreation(view.matchDateTextView, model.gameCreation, model.gameDuration )
            setMatchResult(view.matchScoreTextView,mainView, model, participantId)
            setGameDuration(view.matchLengthTextView, model.gameDuration)
        }
    }

    @JvmStatic
    @BindingAdapter("setChampionInfo", "setParticipantId")
    fun setChampionInfo(view: ConstraintLayout, model: MatchModel, participantId: Int) {
        if(model.gameId != (-1).toLong()){
            setParticipantChampionIcon(view.matchChampionIcon, model, participantId)
            val spell1 = model.participants[participantId - 1].spell1Id
            val spell2 = model.participants[participantId - 1].spell2Id
            val rune1 = model.participants[participantId - 1].stats.perk0
            val rune2 = model.participants[participantId - 1].stats.perkSubStyle
            setSummonerSpells(
                view.matchSpellFirstImageView,
                view.matchSpellSecondImageView,
                spell1,
                spell2
            )
            setRunes(view.matchRuneFirstImageView, view.matchRuneSecondImageView, rune1, rune2)
        }
    }

    @JvmStatic
    @BindingAdapter("setPlayerStats", "setParticipantId")
    fun setPlayerStats(view: LinearLayout, model: MatchModel, participantId: Int) {
        if(model.gameId != (-1).toLong()){
            val stats = model.participants[participantId - 1].stats
            val kills = stats.kills
            val deaths = stats.deaths
            val assists = stats.assists

            view.kdaFullTextView.text = "$kills / $deaths / $assists"
            if (deaths != 0) {
                val kda = (kills.toDouble() + assists.toDouble()) / deaths.toDouble()
                view.kdaTextView.text = "${"%.2f".format(kda)} KDA"
            } else view.kdaTextView.text = "Perfect KDA"
            view.summonerLevelTextView.text = "${stats.champLevel} level"
            view.summonerCsTextView.text = "${stats.totalMinionsKilled} CS"
        }


    }

    private fun setMatchResult(view: TextView, mainView: CardView, match: MatchModel, participantId: Int) {
        var win = false
        if(match.gameDuration <= 300){
            view.text = "Remake"
            mainView.setCardBackgroundColor(Color.parseColor("#858383"))
        }
        else{
            if (match.gameId != (-1).toLong()) {
                if (participantId <= 5) {
                    if (match.teams[0].win.toLowerCase(Locale.ROOT) == "win") {
                        view.text = "Win"
                        win = true
                    }
                    else view.text = "Defeat"
                } else {
                    if (match.teams[1].win.toLowerCase(Locale.ROOT) == "win") {
                        view.text = "Win"
                        win = true
                    }
                    else view.text = "Defeat"
                }
            }
            if(win) mainView.setCardBackgroundColor(Color.parseColor("#98F861"))
            else mainView.setCardBackgroundColor(Color.parseColor("#FF6F6F"))
        }
    }


    private fun setGameDuration(view: TextView, time: Long) {
        val minutes = time / 60
        val seconds = time % 60
        view.text = "${minutes}m ${seconds}s"
    }


    private fun setGameCreation(view: TextView, createTime: Long, duration: Long) {
        val stamp = Timestamp(createTime + (duration * 1000))
        val currentStamp = Timestamp(System.currentTimeMillis())
        val diff = currentStamp.time - stamp.time
        when {
            diff < 60000 -> view.text = "${diff / 1000} second ago"
            diff in 60001 until 3600000 -> view.text = "${diff / 60000} minutes ago"
            diff in 3600001 until 86400000 -> view.text = "${diff / 3600000} hours ago"
            diff in 86400001 until 2678400000 -> view.text = "${diff / 86400000} days ago"
            diff in 2678400001 until 32140800000 -> view.text = "${diff / 2678400000} months ago"
            else -> view.text = "more than years ago"
        }
    }

    private fun setQueue(view: TextView, queue: Int) {
        var queueName = ""
        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.Default) {
                val json = view.context.resources.openRawResource(R.raw.queues)
                    .bufferedReader().use { it.readText() }
                val jsonArray = JSONArray(json)
                for (item in 0 until jsonArray.length()) {
                    val jsonObject = JSONObject(jsonArray[item].toString())
                    if (jsonObject.get("queueId") == queue) {
                        queueName = jsonObject.getString("description")
                        when (queueName) {
                            "5v5 Draft Pick games" -> queueName = "Draft Pick"
                            "5v5 Ranked Solo games" -> queueName = "Ranked Solo/Duo"
                            "5v5 Blind Pick games" -> queueName = "Blind Pick"
                            "5v5 Ranked Flex games" -> queueName = "Ranked Flex"
                            "5v5 ARAM games" -> queueName = "ARAM"
                            "Clash games" -> queueName = "Clash"
                        }
                        if (queueName.contains("Bot", true)) queueName = "Bots"
                        queueName = queueName.replace(" games", "", true)
                        break
                    }
                }
            }
            view.text = queueName
        }
    }

    private fun setRunes(
        primaryRuneView: ImageView,
        secondaryRuneView: ImageView,
        primaryRuneId: Int,
        secondaryRuneId: Int
    ) {
        var primaryRuneRef = ""
        var secondaryRuneRef = ""
        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.Default) {
                val json = primaryRuneView.context.resources.openRawResource(R.raw.runes)
                    .bufferedReader().use { it.readText() }
                val jsonArray = JSONArray(json)
                for (item in 0 until jsonArray.length()) {
                    val jsonObject = JSONObject(jsonArray[item].toString())
                    if (jsonObject.get("id") == primaryRuneId) primaryRuneRef =
                        jsonObject.getString("icon")
                    if (jsonObject.get("id") == secondaryRuneId) secondaryRuneRef =
                        jsonObject.getString("icon")
                }
            }
            if(primaryRuneRef != "") Glide.with(primaryRuneView).load("${BASE_URL}img/$primaryRuneRef").into(primaryRuneView)
            if(secondaryRuneRef != "") Glide.with(secondaryRuneView).load("${BASE_URL}img/$secondaryRuneRef")
                .into(secondaryRuneView)
        }
    }

    private fun setSummonerSpells(
        spell1ImageView: ImageView,
        spell2ImageView: ImageView,
        spell1: Int,
        spell2: Int
    ) {
        var spell1Ref = ""
        var spell2Ref = ""
        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.Default) {
                val json = spell1ImageView.context.resources.openRawResource(R.raw.summoners)
                    .bufferedReader().use { it.readText() }
                val jsonObject = JSONObject(json)
                val data = jsonObject.getJSONObject("data")
                for (item in data.keys()) {
                    val summonerData = JSONObject(data.get(item).toString())
                    if (summonerData.get("key") == spell1.toString()) {
                        val image = summonerData.getJSONObject("image")
                        spell1Ref = image.getString("full")


                    }
                    if (summonerData.get("key") == spell2.toString()) {
                        val image = summonerData.getJSONObject("image")
                        spell2Ref = image.getString("full")
                    }
                }
            }
            if(spell1Ref != "") Glide.with(spell1ImageView).load("$SPELLS_REF$spell1Ref").into(spell1ImageView)
            if(spell2Ref != "")Glide.with(spell1ImageView).load("$SPELLS_REF$spell2Ref").into(spell2ImageView)
        }
    }


    private fun setParticipantChampionIcon(view: ImageView, match: MatchModel, participantId: Int) {
        var championName = ""
        CoroutineScope(Dispatchers.Main).launch{
            withContext(Dispatchers.IO){
                if (match.gameId != (-1).toLong()) championName =
                        getChampionByID(match.participants[participantId - 1].championId.toLong())
            }
            Glide.with(view).load("$CHAMPION_SQUARE_IMG$championName.png").into(view)
        }
    }

}