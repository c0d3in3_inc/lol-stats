package com.c0d3in3.lolstats.api

interface ApiCallback {
    fun onSuccess(response : String, code: Int){}
    fun onError(response : String, code: Int){}
    fun onFail(response: String){}
}