package com.c0d3in3.lolstats.api


import com.c0d3in3.lolstats.base.*
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*

object ApiHandler {

    private val httpClient = OkHttpClient.Builder()
        .build()

    private lateinit var retrofit: Retrofit

    private var currentRegion = ""

    fun getCurrentRegion() : String{
        return currentRegion
    }

    private lateinit var service: NetworkService

    interface NetworkService {
        @Headers("X-Riot-Token:$API_KEY")
        @GET
        fun getRequest(@Url path: String): Call<String>
    }


    fun getRequest(path : String, callback : ApiCallback) {
        val call = service.getRequest(path)
        call.enqueue(onCallback(callback))

    }


    private fun onCallback(callback: ApiCallback): Callback<String> =
        object : Callback<String>{
            override fun onResponse(call: Call<String>, response: Response<String>) {
                val status = response.code()
                println(response.raw())
                if(status == HTTP_200_OK || status == HTTP_201_CREATED) callback.onSuccess(response.body().toString(), status)
                else if(status == HTTP_400_BAD_REQUEST) callback.onError("Bad request", status)
                else if(status == HTTP_204_NO_CONTENT) callback.onError("No content", status)
                else if(status == HTTP_401_UNAUTHORIZED) callback.onError("Unauthorized please log in again", status)
                else if(status == HTTP_404_NOT_FOUND) callback.onError("Resource not found", status)
                else if(status == HTTP_500_INTERNAL_SERVER_ERROR) callback.onError("Internal server error", status)
                else callback.onError("Failed loading data", status)
            }
            override fun onFailure(call: Call<String>, t: Throwable) {
                callback.onFail(t.toString())
            }
        }

    fun createClient(region : String) : Boolean{
        val baseUrl = when(region){
            "EUNE" -> EUNE
            "EUW" -> EUW
            "Brazil" -> BRAZIL
            "North America" -> NORTH_AMERICA
            "Korea" -> KOREA
            else -> EUNE
        }
        currentRegion = region
        retrofit = Retrofit.Builder()
            .addConverterFactory(ScalarsConverterFactory.create())
            .baseUrl(baseUrl)
            .client(httpClient)
            .build()
        service = retrofit.create(NetworkService::class.java)
        return true
    }
}