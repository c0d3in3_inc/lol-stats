package com.c0d3in3.lolstats.base

import android.app.Application

class App : Application() {

    companion object{
        private lateinit var instance : Application

        fun getInstance() : Application{
            return instance
        }
    }

    override fun onCreate() {
        instance = this
        super.onCreate()
    }

    override fun onTerminate() {
        super.onTerminate()
    }
}