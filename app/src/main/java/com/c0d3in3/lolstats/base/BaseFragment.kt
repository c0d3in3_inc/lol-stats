package com.c0d3in3.lolstats.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.c0d3in3.lolstats.R

abstract class BaseFragment : Fragment() {

    var rootView : View? = null

    abstract fun getLayout() : Int

    abstract fun init()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if(rootView == null){
            rootView = inflater.inflate(getLayout(), container, false)
        }
        init()
        return rootView
    }

}