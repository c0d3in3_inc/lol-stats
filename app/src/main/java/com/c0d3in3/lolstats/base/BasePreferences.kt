package com.c0d3in3.lolstats.base

import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences

object BasePreferences {

    private val basePreferences by lazy {
        App.getInstance().applicationContext.getSharedPreferences("APP_PREFS", MODE_PRIVATE)
    }

    private fun getEditor() : SharedPreferences.Editor {
        return basePreferences.edit()
    }

    fun getString(key : String) : String? {
        return basePreferences.getString(key, "null")
    }

    fun setString(key: String, value: String){
        getEditor().putString(key, value).apply()
    }

    fun remove(key: String){
        getEditor().remove(key).apply()
    }

    fun getInt(key : String) : Int? {
        return basePreferences.getInt(key, -1)
    }

    fun setInt(key: String, value: Int){
        getEditor().putInt(key, value).apply()
    }

}