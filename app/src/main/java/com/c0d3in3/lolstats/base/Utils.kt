package com.c0d3in3.lolstats.base

import android.content.Intent
import androidx.core.content.ContextCompat.startActivity
import com.c0d3in3.lolstats.R
import com.c0d3in3.lolstats.api.ApiCallback
import com.c0d3in3.lolstats.api.ApiHandler
import com.c0d3in3.lolstats.model.ChampionModel
import com.c0d3in3.lolstats.model.SummonerModel
import com.c0d3in3.lolstats.ui.splash.SplashActivity
import com.c0d3in3.lolstats.user.UserInfo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONObject

object Utils {

    fun getChampionByID(id: Long): String {
        val json = App.getInstance().resources.openRawResource(R.raw.champion)
            .bufferedReader().use { it.readText() }
        val jsonObject = JSONObject(json)
        val champions = jsonObject.getJSONObject("data")
        val names = champions.names()
        var championName = ""
        for (idx in 0 until champions.length()) {
            val champ = champions.getJSONObject(names[idx].toString())
            if (champ.getString("key").toLong() == id) {
                championName = champ.getString("id")
                break
            }
        }
        return championName
    }

    fun getChampionNameByID(id: Long): String {
        val json = App.getInstance().resources.openRawResource(R.raw.champion)
            .bufferedReader().use { it.readText() }
        val jsonObject = JSONObject(json)
        val champions = jsonObject.getJSONObject("data")
        val names = champions.names()
        var championName = ""
        for (idx in 0 until champions.length()) {
            val champ = champions.getJSONObject(names[idx].toString())
            if (champ.getString("key").toLong() == id) {
                championName = champ.getString("name")
                break
            }
        }
        return championName
    }
}

const val MAX_FAVORITE_CHAMPIONS = 5

const val TEAM_BLUE = 100
const val TEAM_RED = 200

var CURRENT_PATCH = "10.10.4"

const val BASE_URL = "http://ddragon.leagueoflegends.com/cdn/"

const val API_KEY = "RGAPI-8845f372-11f7-43fa-9110-a0a3ad7a4c7e"

const val BRAZIL = "https://br1.api.riotgames.com"
const val EUNE = "https://eun1.api.riotgames.com"
const val EUW = "https://euw1.api.riotgames.com"
const val JAPAN = "https://jp1.api.riotgames.com"
const val KOREA = "https://kr.api.riotgames.com"
const val LATIN_AMERICA_1 = "https://la1.api.riotgames.com"
const val LATIN_AMERICA_2 = "https://la2.api.riotgames.com"
const val NORTH_AMERICA = "https://na1.api.riotgames.com"
const val OCEANIA = "https://oc1.api.riotgames.com"
const val TURKEY = "https://tr1.api.riotgames.com"
const val RUSSIA = "https://ru.api.riotgames.com"

const val RANKED_SOLO_5X5 = "RANKED_SOLO_5x5"
const val RANKED_FLEX_SR = "RANKED_FLEX_SR"
const val RANKED_FLEX_TT = "RANKED_FLEX_TT"

const val CHAMPION_ROTATION = "/lol/platform/v3/champion-rotations"

// ================================= SUMMONERS REFERENCE ==================================//////////////

const val SUMMONER_BARRIER = "SummonerBarrier"
const val SUMMONER_CLEANSE = "SummonerBoost"
const val SUMMONER_IGNITE = "SummonerDot"
const val SUMMONER_FLASH = "SummonerFlash"
const val SUMMONER_GHOST = "SummonerHaste"
const val SUMMONER_HEAL = "SummonerHeal"
const val SUMMONER_CLARITY = "SummonerMana"
const val SUMMONER_PORO_RECALL = "SummonerPoroRecall"
const val SUMMONER_PORO_THROW = "SummonerPoroThrow"
const val SUMMONER_SMITE = "SummonerSmite"
const val SUMMONER_MARK_URF = "SummonerSnowURFSnowball_Mark"
const val SUMMONER_MARK = "SummonerSnowball"
const val SUMMONER_TELEPORT = "SummonerTeleport"

fun syncData(model: SummonerModel){
    val currentUser = FirebaseAuth.getInstance().currentUser
    UserInfo.username = currentUser?.displayName.toString()
    UserInfo.email = currentUser?.email.toString()
    UserInfo.uid = currentUser?.uid.toString()
    UserInfo.summonerId = model.summonerId
    UserInfo.accountId = model.accountId
    UserInfo.summonerLevel = model.summonerLevel
    UserInfo.summonerRegion = model.summonerRegion
    UserInfo.summonerUsername = model.summonerName
    UserInfo.summonerIconId = model.profileIconId
    getLeagueEntries()
    getFavoriteChampion(UserInfo.summonerId)
}

fun saveData(model : SummonerModel) {
    val db = Firebase.firestore
    val auth = FirebaseAuth.getInstance()
    val updateData = mutableMapOf<String, Any>()
    updateData["accountId"] = model.accountId
    updateData["profileIconId"] = model.profileIconId
    updateData["puuid"] = model.puuid
    updateData["revisionDate"] = model.revisionDate
    updateData["summonerId"] = model.summonerId
    updateData["summonerLevel"] = model.summonerLevel
    updateData["summonerName"] = model.summonerName
    updateData["summonerRegion"] = model.summonerRegion
    db.collection("users").document(auth.currentUser!!.displayName.toString()).set(updateData)
}

fun updateUserInfo(model: SummonerModel){
    UserInfo.summonerUsername = model.summonerName
    UserInfo.summonerLevel = model.summonerLevel
    UserInfo.summonerRegion = model.summonerRegion
    UserInfo.summonerIconId = model.profileIconId
    UserInfo.summonerId = model.summonerId
    UserInfo.accountId = model.accountId
}

private fun getFavoriteChampion(summonerId: String) {
    ApiHandler.getRequest("$GET_CHAMPION_MASTERY_ALL$summonerId", object : ApiCallback {
        override fun onSuccess(response: String, code: Int) {
            if (response.isNotEmpty()) {
                val json = JSONArray(response)
                if(json.toString() != "[]"){
                    val championModel = Gson().fromJson(json.getJSONObject(0).toString(), ChampionModel::class.java)
                    UserInfo.favoriteChampion = championModel.championId
                }
                else UserInfo.favoriteChampion = -1
            }
        }
    })
}

private fun getLeagueEntries(){
    ApiHandler.getRequest("$GET_LEAGUE_ENTRIES${UserInfo.summonerId}", object : ApiCallback {
        override fun onSuccess(response: String, code: Int) {
            val jsonArray = JSONArray(response)
            if(jsonArray.toString() == "[]") UserInfo.rank = "UNRANKED"
            else{
                val jsonObject = jsonArray.getJSONObject(0)
                val tier = jsonObject.getString("tier")
                val rank  = jsonObject.getString("rank")
                UserInfo.rank = "$tier $rank"
            }
        }

        override fun onError(response: String, code: Int) {
            UserInfo.rank = "Failed to retrieve rank"
        }

        override fun onFail(response: String) {
            UserInfo.rank = "Failed to retrieve rank"
        }
    })
}

// ================================= END OF SUMMONERS REFERENCE ==================================//////////////

// ================================= IMAGES REFERENCE ==================================//////////////

val SPELLS_REF = "http://ddragon.leagueoflegends.com/cdn/$CURRENT_PATCH/img/spell/"
val ICONS_REF = "http://ddragon.leagueoflegends.com/cdn/$CURRENT_PATCH/img/profileicon/"
val CHAMPION_SQUARE_IMG = "http://ddragon.leagueoflegends.com/cdn/$CURRENT_PATCH/img/champion/"
const val CHAMPION_SPLASH = "http://ddragon.leagueoflegends.com/cdn/img/champion/splash/"

// ================================= END OF IMAGES REFERENCE ==================================//////////////
// ================================= RANK API ==================================//////////////

// /lol/champion-mastery/v4/champion-masteries/by-summoner/{encryptedSummonerId}
const val GET_CHAMPION_MASTERY_ALL = "/lol/champion-mastery/v4/champion-masteries/by-summoner/"

// /lol/champion-mastery/v4/champion-masteries/by-summoner/{encryptedSummonerId}/by-champion/{championId}
const val GET_CHAMPION_MASTERY_BY_CHAMPION =
    "$GET_CHAMPION_MASTERY_ALL/{encryptedSummonerId}/by-champion/{championId}"

const val GET_LEAGUE_ENTRIES = "/lol/league/v4/entries/by-summoner/"

// /lol/league/v4/challengerleagues/by-queue/{queue}
const val GET_CHALLENGERS = "/lol/league/v4/challengerleagues/by-queue/"

// /lol/league/v4/grandmasterleagues/by-queue/{queue}
const val GET_GRANDMASTERS = "/lol/league/v4/grandmasterleagues/by-queue/"

// /lol/league/v4/masterleagues/by-queue/{queue}
const val GET_MASTERS = "/lol/league/v4/masterleagues/by-queue/"

//lol/league/v4/entries/by-summoner/{encryptedSummonerId}
const val GET_SUMMONER_LEAGUE = "/lol/league/v4/entries/by-summoner/"

// ================================= END OF RANK API ==================================//////////////

// ================================= SUMMONER API ==================================//////////////

///lol/summoner/v4/summoners/by-name/{summonerName}
const val GET_SUMMONER_BY_NAME = "/lol/summoner/v4/summoners/by-name/"

// ================================= END OF SUMMONER API ==================================//////////////

// ================================= MATCH API ==================================//////////////

const val GET_MATCH_BY_ID = "/lol/match/v4/matches/"
const val GET_MATCHLIST = "/lol/match/v4/matchlists/by-account/"

// ================================= END OF MATCH API ==================================//////////////


const val HTTP_200_OK = 200
const val HTTP_201_CREATED = 201
const val HTTP_400_BAD_REQUEST = 400
const val HTTP_401_UNAUTHORIZED = 401
const val HTTP_404_NOT_FOUND = 404
const val HTTP_500_INTERNAL_SERVER_ERROR = 500
const val HTTP_204_NO_CONTENT = 204
