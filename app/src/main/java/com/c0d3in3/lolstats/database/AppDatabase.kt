package com.c0d3in3.lolstats.database

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [User::class, Game::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun matchDao() : GameDao
}