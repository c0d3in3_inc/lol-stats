package com.c0d3in3.lolstats.database

import androidx.room.Room
import com.c0d3in3.lolstats.base.App

object Database {

    val instance by lazy{
        Room.databaseBuilder(
            App.getInstance().applicationContext,
            AppDatabase::class.java, "lol-stats"
        ).build()
    }

}