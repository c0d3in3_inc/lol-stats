package com.c0d3in3.lolstats.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.c0d3in3.lolstats.model.MatchModel

@Entity
data class Game(
    @PrimaryKey val gId: Int,
    @ColumnInfo(name = "gameId") val gameId: Long,
    //@ColumnInfo(name = "participantIdentities") val participantIdentities : List<MatchModel.ParticipantIdentityDto>,
    @ColumnInfo(name = "queueId") val queueId : Int,
    @ColumnInfo(name = "gameType") val gameType : String,
    @ColumnInfo(name = "gameDuration") val gameDuration: Long ,
    //@ColumnInfo(name = "teams") val teams : List<MatchModel.TeamStatsDto>,
    @ColumnInfo(name = "platformId") val platformId : String,
    @ColumnInfo(name = "gameCreation") val gameCreation: Long,
    @ColumnInfo(name = "seasonId") val seasonId :Int ,
    @ColumnInfo(name = "gameVersion") val gameVersion : String,
    @ColumnInfo(name = "mapId") val mapId : Int,
    @ColumnInfo(name = "gameMode") val gameMode : String
    //@ColumnInfo(name = "participants") val participants : List<MatchModel.ParticipantDto>
)