package com.c0d3in3.lolstats.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface GameDao {
    @Query("SELECT * FROM game")
    fun getAll(): List<Game>

    @Query("SELECT * FROM game WHERE gameId IN (:gameIds)")
    fun loadAllByIds(gameIds: IntArray): List<Game>

    @Query("SELECT * FROM game WHERE gameId LIKE :gameId LIMIT 1")
    fun findById(gameId: String): Game

    @Insert
    fun insertAll(vararg games: Game)

    @Delete
    fun delete(game: Game)
}