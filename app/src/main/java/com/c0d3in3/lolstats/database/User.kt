package com.c0d3in3.lolstats.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User(
    @PrimaryKey val userId: Int,
    @ColumnInfo(name = "email") val email: String?,
    @ColumnInfo(name = "username") val username: String?,
    @ColumnInfo(name = "password") val password: String?,
    @ColumnInfo(name = "registerDate") val registerDate: String?,
    @ColumnInfo(name = "region") val region: String?,
    @ColumnInfo(name = "summonerUsername") val summonerUsername: String?
)