package com.c0d3in3.lolstats.model

class ChampionModel {
    var championPointsUntilNextLevel : Long =  -1
    var chestGranted = false
    var championId : Long = -1
    var lastPlayTime : Long = -1
    var championLevel = -1
    var summonerId = ""
    var championPoints  = -1
    var championPointsSinceLastLevel : Long = -1
    var tokensEarned = -1
}