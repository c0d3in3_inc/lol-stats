package com.c0d3in3.lolstats.model

class LeagueEntryDTO {
    var leagueId = ""
    var summonerId = ""
    var summonerName = ""
    var queueType = ""
    var tier = ""
    var rank = ""
    var leaguePoints = -1
    var wins = -1
    var losses = -1
    var hotStreak = false
    var veteran	= false
    var freshBlood = false
    var inactive = false
    var miniSeries = MiniSeriesDTO()

    inner class MiniSeriesDTO{
        var losses = -1
        var progress = ""
        var target = -1
        var wins	= -1
    }

}