package com.c0d3in3.lolstats.model

class MatchModel {
    var gameId: Long = 0
    var participantIdentities = listOf<ParticipantIdentityDto>()
    var queueId = 0
    var gameType = ""
    var gameDuration: Long = 0
    var teams = listOf<TeamStatsDto>()
    var platformId = ""
    var gameCreation: Long = 0
    var seasonId = 0
    var gameVersion = ""
    var mapId = 0
    var gameMode = ""
    var participants = listOf<ParticipantDto>()
    var matchUrl = ""

    inner class ParticipantIdentityDto {
        var participantId = 0
        var player = PlayerDto()
    }

    inner class PlayerDto {
        var profileIcon = 0
        var accountId = ""
        var matchHistoryUri = ""
        var currentAccountId = ""
        var currentPlatformId = ""
        var summonerName = ""
        var summonerId = ""
        var platformId = ""
    }

    inner class TeamStatsDto {
        var towerKills = 0
        var riftHeraldKills = 0
        var firstBlood = false
        var inhibitorKills = 0
        var bans = listOf<TeamBansDto>()
        var firstBaron = false
        var firstDragon = false
        var dominionVictoryScore = 0
        var dragonKills = 0
        var baronKills = 0
        var firstInhibitor = false
        var firstTower = false
        var vilemawKills = 0
        var firstRiftHerald = false
        var teamId = 0
        var win = ""
    }

    inner class TeamBansDto {
        var championId = 0
        var pickTurn = 0
    }

    inner class ParticipantDto {
        var participantId = 0
        var championId = 0
        var runes = listOf<RuneDto>()
        var stats = ParticipantStatsDto()
        var teamId = 0
        var timeline = ParticipantTimelineDto()
        var spell1Id = 0
        var spell2Id = 0
        var highestAchievedSeasonTier = ""
        var masteries = listOf<MasteryDto>()

    }

    inner class RuneDto {
        var runeId = 0
        var rank = 0
    }

    inner class ParticipantStatsDto {
        var item0 = 0
        var item2 = 0
        var totalUnitsHealed = 0
        var item1 = 0
        var largestMultiKill = 0
        var goldEarned = 0
        var firstInhibitorKill = false
        var physicalDamageTaken: Long = 0
        var nodeNeutralizeAssist = 0
        var totalPlayerScore = 0
        var champLevel = 0
        var damageDealtToObjectives: Long = 0
        var totalDamageTaken: Long = 0
        var neutralMinionsKilled = 0
        var deaths = 0
        var tripleKills = 0
        var magicDamageDealtToChampions: Long = 0
        var wardsKilled = 0
        var pentaKills = 0
        var damageSelfMitigated: Long = 0
        var largestCriticalStrike = 0
        var nodeNeutralize = 0
        var totalTimeCrowdControlDealt = 0
        var firstTowerKill = false
        var magicDamageDealt: Long = 0
        var totalScoreRank = 0
        var nodeCapture = 0
        var wardsPlaced = 0
        var totalDamageDealt: Long = 0
        var timeCCingOthers: Long = 0
        var magicalDamageTaken: Long = 0
        var largestKillingSpree = 0
        var totalDamageDealtToChampions: Long = 0
        var physicalDamageDealtToChampions: Long = 0
        var neutralMinionsKilledTeamJungle = 0
        var totalMinionsKilled = 0
        var firstInhibitorAssist = false
        var visionWardsBoughtInGame = 0
        var objectivePlayerScore = 0
        var kills = 0
        var firstTowerAssist = false
        var combatPlayerScore = 0
        var inhibitorKills = 0
        var turretKills = 0
        var participantId = 0
        var trueDamageTaken: Long = 0
        var firstBloodAssist = false
        var nodeCaptureAssist = 0
        var assists = 0
        var teamObjective = 0
        var altarsNeutralized = 0
        var goldSpent = 0
        var damageDealtToTurrets: Long = 0
        var altarsCaptured = 0
        var win = false
        var totalHeal: Long = 0
        var unrealKills = 0
        var visionScore: Long = 0
        var physicalDamageDealt: Long = 0
        var firstBloodKill = false
        var longestTimeSpentLiving = 0
        var killingSprees = 0
        var sightWardsBoughtInGame = 0
        var trueDamageDealtToChampions: Long = 0
        var neutralMinionsKilledEnemyJungle = 0
        var doubleKills = 0
        var trueDamageDealt: Long = 0
        var quadraKills = 0
        var item4 = 0
        var item3 = 0
        var item6 = 0
        var item5 = 0
        var playerScore0 = 0
        var playerScore1 = 0
        var playerScore2 = 0
        var playerScore3 = 0
        var playerScore4 = 0
        var playerScore5 = 0
        var playerScore6 = 0
        var playerScore7 = 0
        var playerScore8 = 0
        var playerScore9 = 0
        var perk0 = 0
        var perk0Var1 = 0
        var perk0Var2 = 0
        var perk0Var3 = 0
        var perk1 = 0
        var perk1Var1 = 0
        var perk1Var2 = 0
        var perk1Var3 = 0
        var perk2 = 0
        var perk2Var1 = 0
        var perk2Var2 = 0
        var perk2Var3 = 0
        var perk3 = 0
        var perk3Var1 = 0
        var perk3Var2 = 0
        var perk3Var3 = 0
        var perk4 = 0
        var perk4Var1 = 0
        var perk4Var2 = 0
        var perk4Var3 = 0
        var perk5 = 0
        var perk5Var1 = 0
        var perk5Var2 = 0
        var perk5Var3 = 0
        var perkPrimaryStyle = 0
        var perkSubStyle = 0
    }

    inner class ParticipantTimelineDto {
        var participantId = 0
        var csDiffPerMinDeltas = mapOf<String, Double>()
        var damageTakenPerMinDeltas = mapOf<String, Double>()
        var role = ""
        var damageTakenDiffPerMinDeltas = mapOf<String, Double>()
        var xpPerMinDeltas = mapOf<String, Double>()
        var xpDiffPerMinDeltas = mapOf<String, Double>()
        var lane = ""
        var creepsPerMinDeltas = mapOf<String, Double>()
        var goldPerMinDeltas = mapOf<String, Double>()
    }

    inner class MasteryDto{
        var rank = 0
        var masteryId = 0
    }
}