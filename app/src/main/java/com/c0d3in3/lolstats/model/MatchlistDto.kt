package com.c0d3in3.lolstats.model

class MatchlistDto {
    var startIndex = 0
    var totalGames = 0
    var endIndex = 0
    var matches = arrayListOf<MatchReferenceDto>()

    inner class MatchReferenceDto{
        var gameId : Long = 0
        var role = ""
        var season = 0
        var platformId	= ""
        var champion = 0
        var queue = 0
        var lane = ""
        var timestamp : Long = 0
    }
}