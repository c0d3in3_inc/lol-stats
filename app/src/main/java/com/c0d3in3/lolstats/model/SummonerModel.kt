package com.c0d3in3.lolstats.model

import com.google.gson.annotations.SerializedName

class SummonerModel {
    var accountId = ""
    var profileIconId = 0
    var revisionDate : Long = 0
    @SerializedName("name")
    var summonerName = ""
    @SerializedName("id")
    var summonerId = ""
    var puuid = ""
    var summonerLevel : Long = 0
    var summonerRegion = ""
}