package com.c0d3in3.lolstats.model

class UserDataResult {
    var matchListDto : MatchlistDto? = null
    var matchList : ArrayList<MatchlistDto.MatchReferenceDto>? = null
    var matches : ArrayList<MatchModel>? = null
    var favoriteChamps:ArrayList<ChampionModel>? = null
}