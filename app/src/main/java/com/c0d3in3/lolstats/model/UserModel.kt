package com.c0d3in3.lolstats.model

import android.os.Parcel
import android.os.Parcelable

class UserModel() : Parcelable {
    var email = ""
    var username = ""
    var password = ""
    var registerDate = ""
    var region = ""
    var summonerUsername = ""

    constructor(parcel: Parcel) : this() {
        email = parcel.readString().toString()
        username = parcel.readString().toString()
        password = parcel.readString().toString()
        registerDate = parcel.readString().toString()
        region = parcel.readString().toString()
        summonerUsername = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(email)
        parcel.writeString(username)
        parcel.writeString(password)
        parcel.writeString(registerDate)
        parcel.writeString(region)
        parcel.writeString(summonerUsername)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserModel> {
        override fun createFromParcel(parcel: Parcel): UserModel {
            return UserModel(parcel)
        }

        override fun newArray(size: Int): Array<UserModel?> {
            return arrayOfNulls(size)
        }
    }
}