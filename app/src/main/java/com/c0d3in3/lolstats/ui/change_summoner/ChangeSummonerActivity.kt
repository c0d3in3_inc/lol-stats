package com.c0d3in3.lolstats.ui.change_summoner

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.c0d3in3.lolstats.R
import com.c0d3in3.lolstats.api.ApiCallback
import com.c0d3in3.lolstats.api.ApiHandler
import com.c0d3in3.lolstats.base.GET_SUMMONER_BY_NAME
import com.c0d3in3.lolstats.base.saveData
import com.c0d3in3.lolstats.base.syncData
import com.c0d3in3.lolstats.model.SummonerModel
import com.c0d3in3.lolstats.ui.splash.SplashActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.coroutines.coroutineContext

class ChangeSummonerActivity : AppCompatActivity() {

    var summonerName = ""
    var region = ""
    private lateinit var summonerModel : SummonerModel
    private var summonerFound = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_search)
        init()
    }

    private fun updateUI(){
        headerTextView.text = "Change summoner"
        searchLayout.visibility = View.VISIBLE
        headerLayout.visibility = View.VISIBLE
        searchContainer.visibility = View.VISIBLE
        searchButton.text = "Change"
    }

    private fun init(){

        updateUI()

        searchBackButton.setOnClickListener {
            finish()
        }
        ArrayAdapter.createFromResource(
            this,
            R.array.regions,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            regionSpinner.adapter = adapter
        }

        regionSpinner.onItemSelectedListener

        regionSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val selectedItem = parent.getItemAtPosition(position).toString()
                if(selectedItem != region) summonerFound = false
                region = selectedItem
            } // to close the onItemSelected

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        searchButton.setOnClickListener {
            checkSummoner()
        }
    }

    private fun change(){
        if(!summonerFound) return Toast.makeText(this, "Search for summoner to continue!", Toast.LENGTH_SHORT).show()

        CoroutineScope(Dispatchers.IO).launch{
            withContext(Dispatchers.IO){
                syncData(summonerModel)
                saveData(summonerModel)
                val intent = Intent(this@ChangeSummonerActivity, SplashActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }
        }
    }

    private fun checkSummoner(){
        if(region != ApiHandler.getCurrentRegion()) ApiHandler.createClient(region)
        summonerName = summonerEditText.text.toString()
        infoMessage.visibility = View.VISIBLE
        ApiHandler.getRequest("$GET_SUMMONER_BY_NAME$summonerName", object : ApiCallback {
            override fun onSuccess(response: String, code: Int) {
                infoMessage.setTextColor(Color.GREEN)
                summonerFound = true
                summonerModel = Gson().fromJson(response, SummonerModel::class.java)
                summonerModel.summonerRegion = region
                change()
            }

            override fun onError(response: String, code: Int) {
                infoMessage.setTextColor(Color.RED)
                summonerFound = false
                infoMessage.text = getString(R.string.summoner_isn_t_found_try_again)
            }
        })
    }
}
