package com.c0d3in3.lolstats.ui.dashboard

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import com.c0d3in3.lolstats.R
import com.c0d3in3.lolstats.base.BasePagerAdapter
import com.c0d3in3.lolstats.ui.dashboard.home.HomeFragment
import com.c0d3in3.lolstats.ui.dashboard.profile.ProfileFragment
import com.c0d3in3.lolstats.ui.dashboard.search.SearchFragment
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : AppCompatActivity() {

    private val adapter = BasePagerAdapter(this)

    private var isNavViewClickable = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        adapter.addFragment(HomeFragment(null))
        adapter.addFragment(SearchFragment())
        adapter.addFragment(ProfileFragment())

        init()
        viewPager.adapter = adapter
    }

    private fun init(){
        addViewPagerListener()
        addNavMenuListener()
    }

    private fun addNavMenuListener(){
        navView.setOnNavigationItemSelectedListener {
            if(isNavViewClickable){
                when(it.itemId){
                    R.id.nav_home -> viewPager.setCurrentItem(0, true)
                    R.id.nav_search -> viewPager.setCurrentItem(1, true)
                    R.id.nav_profile -> viewPager.setCurrentItem(2, true)
                }

                true
            }
            else false

        }
    }

    fun bottomNavViewToggle(value : Boolean){
        isNavViewClickable = value
    }


    private fun addViewPagerListener(){
        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                navView.menu.getItem(position).isChecked = true
            }

        })
    }

}
