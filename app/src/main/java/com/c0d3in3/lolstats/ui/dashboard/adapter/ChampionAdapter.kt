package com.c0d3in3.lolstats.ui.dashboard.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.c0d3in3.lolstats.R
import com.c0d3in3.lolstats.databinding.ChampionItemLayoutBinding
import com.c0d3in3.lolstats.model.ChampionModel
import com.c0d3in3.lolstats.user.UserInfo

class ChampionAdapter(private val champions : ArrayList<ChampionModel>) : RecyclerView.Adapter<ChampionAdapter.ViewHolder>(){

    inner class ViewHolder(private val binding : ChampionItemLayoutBinding) : RecyclerView.ViewHolder(binding.root){
        fun onBind(){
            binding.championModel = champions[adapterPosition]
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding : ChampionItemLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.champion_item_layout, parent, false)
        return ViewHolder(binding)
    }


    override fun getItemCount() = champions.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }
}