package com.c0d3in3.lolstats.ui.dashboard.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.c0d3in3.lolstats.R
import com.c0d3in3.lolstats.databinding.MatchItemLayoutBinding
import com.c0d3in3.lolstats.model.MatchModel
import com.c0d3in3.lolstats.model.SummonerModel

class MatchAdapter(
    private val matches: ArrayList<MatchModel>,
    private val callback: MatchCallback,private val summonerInfo: SummonerModel) : RecyclerView.Adapter<MatchAdapter.ViewHolder>() {

    interface MatchCallback {
        fun loadMatchAgain(position: Int)
    }

    inner class ViewHolder(private val binding: MatchItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            binding.summonerName = summonerInfo.summonerName

            matches[adapterPosition].participantIdentities.forEach {
                if (it.player.summonerId == summonerInfo.summonerId) binding.participantId = it.participantId
            }

            binding.matchModel = matches[adapterPosition]
            if (matches[adapterPosition].gameId == (-1).toLong()) {
                binding.matchLayout.visibility = View.GONE
                binding.failedTextView.visibility = View.VISIBLE
            } else {
                binding.matchLayout.visibility = View.VISIBLE
                binding.failedTextView.visibility = View.GONE
            }
            binding.matchCardView.setOnClickListener {
                if (matches[adapterPosition].gameId == (-1).toLong()) callback.loadMatchAgain(
                    adapterPosition
                )
            }
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: MatchItemLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.match_item_layout,
            parent,
            false
        )
        return ViewHolder(binding)
    }


    override fun getItemCount() = matches.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }
}