package com.c0d3in3.lolstats.ui.dashboard.home

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.c0d3in3.lolstats.R
import com.c0d3in3.lolstats.api.ApiCallback
import com.c0d3in3.lolstats.api.ApiHandler
import com.c0d3in3.lolstats.base.*
import com.c0d3in3.lolstats.model.ChampionModel
import com.c0d3in3.lolstats.model.MatchModel
import com.c0d3in3.lolstats.model.MatchlistDto
import com.c0d3in3.lolstats.model.SummonerModel
import com.c0d3in3.lolstats.ui.dashboard.DashboardActivity
import com.c0d3in3.lolstats.ui.dashboard.adapter.ChampionAdapter
import com.c0d3in3.lolstats.ui.dashboard.adapter.MatchAdapter
import com.c0d3in3.lolstats.user.UserInfo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.include_progressbar_layout.view.*
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import org.json.JSONArray

class HomeFragment(private val summonerModel: SummonerModel?) : BaseFragment(),
    MatchAdapter.MatchCallback {

    private val matches = arrayListOf<MatchModel>()
    private lateinit var matchAdapter: MatchAdapter

    private val favoriteChampions = arrayListOf<ChampionModel>()
    private lateinit var favoriteChampionAdapter: ChampionAdapter

    private var loadingMatches = false

    private var summonerInfo = SummonerModel()


    override fun getLayout() = R.layout.fragment_home

    override fun init() {

        if (summonerModel != null) summonerInfo = summonerModel
        else {
            summonerInfo.summonerId = UserInfo.summonerId
            summonerInfo.accountId = UserInfo.accountId
            summonerInfo.summonerLevel = UserInfo.summonerLevel
            summonerInfo.summonerRegion = UserInfo.summonerRegion
            summonerInfo.summonerName = UserInfo.summonerUsername
            summonerInfo.profileIconId = UserInfo.summonerIconId
        }
        loadUserUI()

        rootView!!.homeSwipeRefreshLayout.setOnRefreshListener {
            getNetworkData()
        }
    }

    private fun refreshData(summonerName : String){
        ApiHandler.getRequest("$GET_SUMMONER_BY_NAME${summonerName}", object : ApiCallback {
            override fun onSuccess(response: String, code: Int) {
                val region = summonerInfo.summonerRegion
                val summonerModel = Gson().fromJson(response, SummonerModel::class.java)
                summonerModel.summonerRegion = region
                if(summonerModel.accountId == UserInfo.accountId){
                    updateUserInfo(summonerModel)
                    saveData(summonerModel)
                }
                else summonerInfo = summonerModel
                updateUserProfile()
            }
        })
    }

    private fun updateUserProfile(){
        Glide.with(context!!).load("$ICONS_REF${summonerInfo.profileIconId}.png")
            .apply(RequestOptions.circleCropTransform()).into(rootView!!.summonerIconImageView)
        rootView!!.summonerUsernameTextView.text =
            "${summonerInfo.summonerName} • ${summonerInfo.summonerRegion}"

        rootView!!.summonerLevelTextView.text = "${summonerInfo.summonerLevel}"
    }


    private fun loadUserUI() {
        updateUserProfile()
        val layoutManager = LinearLayoutManager(context)
        rootView!!.matchesRecyclerView.layoutManager = layoutManager
        matchAdapter = MatchAdapter(matches, this, summonerInfo)
        rootView!!.matchesRecyclerView.adapter = matchAdapter
        rootView!!.matchesRecyclerView.isNestedScrollingEnabled = false

        rootView!!.homeScrollLayout.setOnScrollChangeListener { v: NestedScrollView?, _: Int, scrollY: Int, _: Int, oldScrollY: Int ->
            if (v != null) {
                if (v.getChildAt(v.childCount - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.childCount - 1).measuredHeight - v.measuredHeight)) &&
                        scrollY > oldScrollY
                    ) {
                        if (!loadingMatches) {
                            loadingMatches = true
                            loadMatches()
                        }
                    }
                }
            }
        }


        rootView!!.favoriteChampsRecyclerView.layoutManager =
            GridLayoutManager(context, MAX_FAVORITE_CHAMPIONS)
        favoriteChampionAdapter = ChampionAdapter(favoriteChampions)
        rootView!!.favoriteChampsRecyclerView.adapter = favoriteChampionAdapter

        getNetworkData()
    }

    private fun getChampions(summonerId: String) {
        ApiHandler.getRequest("$GET_CHAMPION_MASTERY_ALL$summonerId", object : ApiCallback {
            override fun onSuccess(response: String, code: Int) {
                getFavoriteChampions(response)
            }
        })
    }

    private fun loadingMode(loading: Boolean) {
        if (loading) animateView(rootView!!.progress_overlay, View.VISIBLE, 0.6f, 200)
        else animateView(rootView!!.progress_overlay, View.GONE, 0f, 200)
    }

    private fun loadMatches() {
        loadingMode(loadingMatches)
        val scope = CoroutineScope(IO).launch {
            withContext(IO) {
                if (ApiHandler.getCurrentRegion() != summonerInfo.summonerRegion) ApiHandler.createClient(
                    summonerInfo.summonerRegion
                )
            }
            withContext(IO) {
                getMatchlist(summonerInfo.accountId, matches.size)
            }
        }
        if (scope.isCompleted) scope.cancel()
    }

    private fun getNetworkData() {

        val scope = CoroutineScope(IO).launch {
            (activity as DashboardActivity).bottomNavViewToggle(false)
            withContext(Main) {
                if (ApiHandler.getCurrentRegion() != summonerInfo.summonerRegion) ApiHandler.createClient(
                    summonerInfo.summonerRegion
                )
                matches.clear()
                favoriteChampions.clear()
                matchAdapter.notifyDataSetChanged()
                favoriteChampionAdapter.notifyDataSetChanged()
            }
            refreshData(summonerInfo.summonerName)
            loadingMode(true)
            getChampions(summonerInfo.summonerId)
            getMatchlist(summonerInfo.accountId, 0)

        }

        if (scope.isCompleted) scope.cancel()
    }


    private fun getFavoriteChampions(data: String) {
        if (data.isNotEmpty()) {
            val json = JSONArray(data)
            for (idx in 0 until json.length()) {
                if(idx == MAX_FAVORITE_CHAMPIONS) break
                val championModel = Gson().fromJson(
                    json.getJSONObject(idx).toString(),
                    ChampionModel::class.java
                )
                favoriteChampions.add(championModel)
            }
            favoriteChampionAdapter.notifyDataSetChanged()
        }
    }

    private fun getMatchlist(summonerId: String, startIndex: Int) {
        var matchlistDto: MatchlistDto
        val previousSize = matches.size
        ApiHandler.getRequest("$GET_MATCHLIST$summonerId?beginIndex=$startIndex&endIndex=${startIndex + 10}", object : ApiCallback {
            override fun onSuccess(response: String, code: Int) {
                matchlistDto = Gson().fromJson(response, MatchlistDto::class.java)
                val matchList = matchlistDto.matches
                val mList = arrayListOf<MatchModel>()
                if(matches.size >= matchlistDto.totalGames) onFinished(previousSize, mList)
                else{
                    val gamesNumber: Int = if(matchlistDto.totalGames - startIndex < 10) matchlistDto.totalGames - startIndex
                    else 10
                    var downloaded = 0
                    for (match in 0 until gamesNumber) {
                        val id = matchList[match].gameId
                        ApiHandler.getRequest("$GET_MATCH_BY_ID$id", object : ApiCallback {
                            override fun onSuccess(response: String, code: Int) {
                                val matchModel = Gson().fromJson(response, MatchModel::class.java)
                                mList.add(matchModel)
                                downloaded++
                                if(downloaded == matchList.size) onFinished(previousSize, mList)
                            }

                            override fun onFail(response: String) {
                                val matchModel = MatchModel()
                                matchModel.gameId = -1
                                matchModel.matchUrl = "$GET_MATCH_BY_ID${id}"
                                mList.add(matchModel)
                                onFinished(previousSize, mList)
                            }

                            override fun onError(response: String, code: Int) {
                                val matchModel = MatchModel()
                                matchModel.gameId = -1
                                matchModel.matchUrl = "$GET_MATCH_BY_ID${id}"
                                mList.add(matchModel)
                                onFinished(previousSize, mList)
                            }
                        })
                    }
                }
            }

            override fun onError(response: String, code: Int) {
                onFinished(previousSize, null)
            }

            override fun onFail(response: String) {
                onFinished(previousSize, null)
            }
        })
    }

    private fun onFinished(previousSize: Int, mList: ArrayList<MatchModel>?) {
        if(rootView!!.homeSwipeRefreshLayout.isRefreshing)  rootView!!.homeSwipeRefreshLayout.isRefreshing = false
        if (mList != null) {
            if(mList.size == 0 && matches.size == 0){
                rootView!!.emptyTextView.visibility = View.VISIBLE
                rootView!!.matchesRecyclerView.visibility = View.GONE
            }
            else{
                if(rootView!!.emptyTextView.visibility == View.VISIBLE){
                    rootView!!.emptyTextView.visibility = View.GONE
                    rootView!!.matchesRecyclerView.visibility = View.VISIBLE
                }
                mList.sortByDescending { it.gameCreation }
                matches.addAll(mList)
                matchAdapter.notifyItemRangeInserted(previousSize, 10)
                loadingMatches = false
                loadingMode(loadingMatches)
            }
        }
        else{
            if(matches.size == 0){
                rootView!!.emptyTextView.visibility = View.VISIBLE
                rootView!!.matchesRecyclerView.visibility = View.GONE
            }
            loadingMatches = false
            loadingMode(loadingMatches)
        }
        (activity as DashboardActivity).bottomNavViewToggle(true)
    }

    override fun loadMatchAgain(position: Int) {
        ApiHandler.getRequest(
            matches[position].matchUrl,
            object : ApiCallback {
                override fun onSuccess(response: String, code: Int) {
                    val matchModel =
                        Gson().fromJson(response, MatchModel::class.java)
                    matches[position] = matchModel
                    matchAdapter.notifyItemChanged(position)
                }
            })
    }


    private fun animateView(view: View, toVisibility: Int, toAlpha: Float, duration: Long) {
        val show = toVisibility == View.VISIBLE;
        if (show) {
            view.alpha = 0F
            rootView!!.homeScrollLayout.alpha = 0.3f
            view.animate().setDuration(duration).alpha(toAlpha)
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        view.visibility = toVisibility;
                    }
                })
        } else {
            rootView!!.homeScrollLayout.alpha = 1.0f
            view.animate().setDuration(duration).alpha(0f)
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        view.visibility = toVisibility;
                    }
                })
        }
    }
}