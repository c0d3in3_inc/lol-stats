package com.c0d3in3.lolstats.ui.dashboard.profile

import android.content.Intent
import android.graphics.Color
import com.bumptech.glide.Glide
import com.c0d3in3.lolstats.R
import com.c0d3in3.lolstats.base.*
import com.c0d3in3.lolstats.ui.change_summoner.ChangeSummonerActivity
import com.c0d3in3.lolstats.ui.dashboard.DashboardActivity
import com.c0d3in3.lolstats.ui.sign.SignActivity
import com.c0d3in3.lolstats.user.UserInfo
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_profile.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ProfileFragment : BaseFragment() {
    override fun getLayout() = R.layout.fragment_profile

    override fun init() {
        loadSummonerUI()
    }

    private fun loadSummonerUI(){
        println("gamodzaxeba")
        println("${UserInfo.favoriteChampion } champi")
        if(UserInfo.favoriteChampion != (-1).toLong()){
            var championName = ""
            CoroutineScope(Dispatchers.Main).launch {
                withContext(Dispatchers.Default){
                    championName = Utils.getChampionByID(UserInfo.favoriteChampion)
                }
                Glide.with(context!!).load("$CHAMPION_SPLASH${championName}_0.jpg").fitCenter().into(rootView!!.summonerSplashImageView)
            }
        }
        else rootView!!.summonerSplashImageView.setBackgroundColor(Color.BLACK)
        Glide.with(context!!).load("$ICONS_REF${UserInfo.summonerIconId}.png").circleCrop().into(rootView!!.summonerIconImageView)

        val auth = FirebaseAuth.getInstance().currentUser
        rootView!!.welcomeTextView.text = "You are currently logged as ${auth?.displayName}"
        rootView!!.summonerNameTextView.text = UserInfo.summonerUsername
        rootView!!.summonerLevelTextView.text = UserInfo.summonerLevel.toString()
        rootView!!.summonerRankTextView.text = "${UserInfo.summonerRegion} • ${UserInfo.rank}"

        rootView!!.changeSummonerTextView.setOnClickListener {
            val intent = Intent((activity as DashboardActivity), ChangeSummonerActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            (activity as DashboardActivity).startActivity(intent)
        }
        rootView!!.signOutTextView.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            val intent = Intent(activity as DashboardActivity, SignActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            (activity as DashboardActivity).startActivity(intent)

        }
    }

}