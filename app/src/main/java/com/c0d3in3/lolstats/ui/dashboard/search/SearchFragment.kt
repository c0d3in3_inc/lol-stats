package com.c0d3in3.lolstats.ui.dashboard.search

import android.graphics.Color
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.c0d3in3.lolstats.R
import com.c0d3in3.lolstats.api.ApiCallback
import com.c0d3in3.lolstats.api.ApiHandler
import com.c0d3in3.lolstats.base.BaseFragment
import com.c0d3in3.lolstats.base.GET_SUMMONER_BY_NAME
import com.c0d3in3.lolstats.model.SummonerModel
import com.c0d3in3.lolstats.ui.dashboard.DashboardActivity
import com.c0d3in3.lolstats.ui.dashboard.home.HomeFragment
import com.c0d3in3.lolstats.user.UserInfo
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_search.view.*
import java.util.*

class SearchFragment : BaseFragment() {

    private var region = ""
    private var summonerName = ""
    private lateinit var summonerModel : SummonerModel
    private var summonerFound = false

    override fun getLayout() = R.layout.fragment_search

    override fun init() {
        rootView!!.searchLayout.visibility = View.VISIBLE
        rootView!!.headerLayout.visibility = View.GONE
        rootView!!.searchContainer.visibility = View.VISIBLE
        ArrayAdapter.createFromResource(
            context!!,
            R.array.regions,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            rootView!!.regionSpinner.adapter = adapter
        }

        rootView!!.regionSpinner.onItemSelectedListener

        rootView!!.regionSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val selectedItem = parent.getItemAtPosition(position).toString()
                if(selectedItem != region) summonerFound = false
                region = selectedItem
            } // to close the onItemSelected

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        rootView!!.searchButton.setOnClickListener {
            if((region == UserInfo.summonerRegion && rootView!!.summonerEditText.text.toString()
                    .toLowerCase(Locale.ROOT) == UserInfo.summonerUsername.toLowerCase(Locale.ROOT)) ||
                rootView!!.summonerEditText.text.toString().isBlank()) return@setOnClickListener Toast.makeText(context, "Text is empty or you're searching for yourself, fool",Toast.LENGTH_SHORT).show()
            else  checkSummoner()
        }

        rootView!!.searchBackButton.setOnClickListener {
            rootView!!.searchLayout.visibility = View.VISIBLE
            rootView!!.headerLayout.visibility = View.GONE
            rootView!!.searchContainer.visibility = View.VISIBLE
        }
    }

    private fun search(){
        if(!summonerFound) return Toast.makeText(context, "Search for summoner to continue!", Toast.LENGTH_SHORT).show()
        rootView!!.searchLayout.visibility = View.GONE
        rootView!!.headerLayout.visibility = View.VISIBLE
        rootView!!.searchContainer.visibility = View.GONE
        val transaction = (activity as DashboardActivity).supportFragmentManager.beginTransaction()
        transaction.replace(R.id.searchFrame, HomeFragment(summonerModel))
        transaction.commit()
    }

    private fun checkSummoner(){
        if(region != ApiHandler.getCurrentRegion()) ApiHandler.createClient(region)
        summonerName = rootView!!.summonerEditText.text.toString()
        rootView!!.infoMessage.visibility = View.VISIBLE
        ApiHandler.getRequest("$GET_SUMMONER_BY_NAME$summonerName", object : ApiCallback {
            override fun onSuccess(response: String, code: Int) {
                rootView!!.infoMessage.setTextColor(Color.GREEN)
                summonerFound = true
                rootView!!.infoMessage.text = "Summoner $summonerName is found. Do you want to process?\nIf not, search again for another summoner!"
                summonerModel = Gson().fromJson(response, SummonerModel::class.java)
                summonerModel.summonerRegion = region
                search()
            }

            override fun onError(response: String, code: Int) {
                rootView!!.infoMessage.setTextColor(Color.RED)
                summonerFound = false
                rootView!!.infoMessage.text = getString(R.string.summoner_isn_t_found_try_again)
            }
        })
    }
}