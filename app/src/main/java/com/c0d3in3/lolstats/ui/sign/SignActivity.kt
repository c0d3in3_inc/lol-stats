package com.c0d3in3.lolstats.ui.sign

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.c0d3in3.lolstats.R
import com.c0d3in3.lolstats.base.BasePagerAdapter
import kotlinx.android.synthetic.main.activity_sign.*

class SignActivity : AppCompatActivity() {

    val signAdapter = BasePagerAdapter(this)

    private fun init() {
        signAdapter.addFragment(SignInFragment())
        signAdapter.addFragment(SignUpFragment())
        signPager.adapter = signAdapter
    }

    override fun onBackPressed() {
        if (signPager.currentItem == 0) super.onBackPressed()
        else signPager.currentItem = signPager.currentItem - 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign)

        init()
    }
}
