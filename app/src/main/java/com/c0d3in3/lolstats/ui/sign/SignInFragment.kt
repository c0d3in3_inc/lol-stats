package com.c0d3in3.lolstats.ui.sign

import android.content.Intent
import android.util.Log.d
import android.util.Log.w
import android.widget.Toast
import com.c0d3in3.lolstats.R
import com.c0d3in3.lolstats.api.ApiHandler
import com.c0d3in3.lolstats.base.BaseFragment
import com.c0d3in3.lolstats.base.BasePreferences
import com.c0d3in3.lolstats.base.syncData
import com.c0d3in3.lolstats.model.SummonerModel
import com.c0d3in3.lolstats.ui.dashboard.DashboardActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.activity_sign.*
import kotlinx.android.synthetic.main.fragment_sign_in.*
import kotlinx.android.synthetic.main.fragment_sign_in.view.*
import kotlinx.android.synthetic.main.fragment_sign_in.view.rememberCheckBox

class SignInFragment : BaseFragment() {

    override fun init() {
        if(BasePreferences.getString("email") != "null") {
            rootView!!.rememberCheckBox.isChecked = true
            rootView!!.emailEditText.setText(BasePreferences.getString("email"))
        }
        rootView!!.signInButton.setOnClickListener {
            signIn()
        }

        rootView!!.signUpTextView.setOnClickListener {
            (activity as SignActivity).signPager.currentItem = 1
        }
    }

    override fun getLayout() = R.layout.fragment_sign_in

    private fun signIn(){
        // Initialize Firebase Auth
        val auth = FirebaseAuth.getInstance()

        val email = rootView!!.emailEditText.text.toString()
        val password = rootView!!.passwordEditText.text.toString()

        if(rememberCheckBox.isChecked){
            BasePreferences.setString("email", email)
        }
        if(BasePreferences.getString("email") != null && !rememberCheckBox.isChecked) BasePreferences.remove("email")

        if(email.isEmpty() || password.isEmpty()) return Toast.makeText(context, "Fields must not be empty!", Toast.LENGTH_SHORT).show()

        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(activity as SignActivity) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    d("signIn", "signInWithEmail:success")
                    val db = Firebase.firestore
                    db.collection("users").document(auth.currentUser!!.displayName.toString()).get().addOnSuccessListener {
                        val summonerModel = it.toObject(SummonerModel::class.java)
                        if (summonerModel != null) {
                            ApiHandler.createClient(summonerModel.summonerRegion)
                            syncData(summonerModel)
                            val intent = Intent(context, DashboardActivity::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                        }
                    }
                } else {
                    // If sign in fails, display a message to the user.
                    w("signIn", "signInWithEmail:failure", task.exception)
                    Toast.makeText(context, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }

                // ...
            }
    }
}