package com.c0d3in3.lolstats.ui.sign

import android.content.Intent
import android.widget.Toast
import com.c0d3in3.lolstats.R
import com.c0d3in3.lolstats.base.BaseFragment
import com.c0d3in3.lolstats.model.UserModel
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign.*
import kotlinx.android.synthetic.main.fragment_sign_up.view.*
import java.text.SimpleDateFormat
import java.util.*

class SignUpFragment : BaseFragment() {


    override fun init() {
        rootView!!.continueSignUpButton.setOnClickListener {
            signUp()
        }

        rootView!!.signInTextView.setOnClickListener {
            (activity as SignActivity).signPager.currentItem = 0
        }
    }

    override fun getLayout() = R.layout.fragment_sign_up

    private fun signUp(){
        val db = Firebase.firestore
        var usernameExists: Boolean


        val email = rootView!!.emailSignUpEditText.text.toString()
        val password = rootView!!.passwordSignUpEditText.text.toString()
        val confirmPassword = rootView!!.confirmPasswordEditText.text.toString()
        val username = rootView!!.usernameEditText.text.toString()


        if(email.isEmpty() || username.isEmpty() || password.isEmpty()) return Toast.makeText(context, "Fields must not be empty!", Toast.LENGTH_SHORT).show()
        if(password != confirmPassword) return Toast.makeText(context, "Passwords doesn't match!", Toast.LENGTH_SHORT).show()
        if(password.length < 6 || password.length > 32) return Toast.makeText(context, "Password length must be min 6 and max 32!",Toast.LENGTH_SHORT).show()
        val sdf = SimpleDateFormat("dd/M/yyyy", Locale.getDefault())
        val currentDate = sdf.format(Date())


        db.collection("users").document(username).get().addOnSuccessListener {
            usernameExists = it.exists()
            if(usernameExists) Toast.makeText(context, "Username is taken!", Toast.LENGTH_SHORT).show()
            else{
                val userModel = UserModel()
                userModel.email = email
                userModel.password = password
                userModel.username = username
                userModel.registerDate = currentDate

                val intent = Intent((activity as SignActivity), SignUpSummonerActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("userModel", userModel)
                (activity as SignActivity).startActivity(intent)
            }
        }

    }
}