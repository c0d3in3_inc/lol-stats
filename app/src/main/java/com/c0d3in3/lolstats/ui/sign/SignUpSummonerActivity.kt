package com.c0d3in3.lolstats.ui.sign

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.c0d3in3.lolstats.R
import com.c0d3in3.lolstats.api.ApiCallback
import com.c0d3in3.lolstats.api.ApiHandler
import com.c0d3in3.lolstats.base.GET_SUMMONER_BY_NAME
import com.c0d3in3.lolstats.base.syncData
import com.c0d3in3.lolstats.model.SummonerModel
import com.c0d3in3.lolstats.model.UserModel
import com.c0d3in3.lolstats.ui.dashboard.DashboardActivity
import com.c0d3in3.lolstats.user.UserInfo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_sign_up_summoner.*


class SignUpSummonerActivity : AppCompatActivity() {

    private var region = ""
    private var summonerName = ""
    private lateinit var userModel : UserModel
    private lateinit var summonerModel : SummonerModel
    private var summonerFound = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_summoner)
        userModel = intent.extras?.get("userModel") as UserModel
        init()
    }

    private fun init() {
        ArrayAdapter.createFromResource(
            baseContext,
            R.array.regions,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            regionSpinner.adapter = adapter
        }

        regionSpinner.onItemSelectedListener

        regionSpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val selectedItem = parent.getItemAtPosition(position).toString()
                if(selectedItem != region) summonerFound = false
                region = selectedItem
            } // to close the onItemSelected

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        signUpButton.setOnClickListener {
            if(summonerName != summonerEditText.text.toString() || !summonerFound) checkSummoner()
            else signUp()
        }
    }

    private fun signUp(){
        if(!summonerFound) return Toast.makeText(this, "To finish sign up, please choose your summoner!", Toast.LENGTH_SHORT).show()
        val auth = FirebaseAuth.getInstance()
        val db = Firebase.firestore
        auth.createUserWithEmailAndPassword(userModel.email, userModel.password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("signUp", "createUserWithEmail:success")

                    val user = FirebaseAuth.getInstance().currentUser

                    val profileUpdates = userProfileChangeRequest {
                        displayName = userModel.username
                    }

                    user!!.updateProfile(profileUpdates)
                        .addOnCompleteListener { mTask ->
                            if (mTask.isSuccessful) {
                                Log.d("userProfileChange", "User profile updated.")
                            }
                        }

                    val userData = hashMapOf(
                        "summonerName" to summonerModel.summonerName,
                        "accountId" to summonerModel.accountId,
                        "profileIconId" to summonerModel.profileIconId,
                        "revisionDate" to summonerModel.revisionDate,
                        "summonerId" to summonerModel.summonerId,
                        "puuid" to summonerModel.puuid,
                        "summonerLevel" to summonerModel.summonerLevel,
                        "summonerRegion" to summonerModel.summonerRegion
                    )

                    syncData(summonerModel)

                    // Add a new document with a generated ID
                    db.collection("users").document(userModel.username)
                        .set(userData)
                        .addOnSuccessListener {
                            Log.d(
                                "AddUserData",
                                "DocumentSnapshot added with ID: ${userModel.username} successfully"
                            )
                        }
                        .addOnFailureListener { e ->
                            Log.w("AddUserData", "Error adding document", e)
                        }

                    val intent = Intent(this, DashboardActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                } else {
                    Log.w("signUp", "createUserWithEmail:failure", task.exception)
                    Toast.makeText(this, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }

                // ...
            }
    }

    private fun checkSummoner(){
        ApiHandler.createClient(region)
        summonerName = summonerEditText.text.toString()
        infoMessage.visibility = View.VISIBLE
        ApiHandler.getRequest("$GET_SUMMONER_BY_NAME$summonerName", object : ApiCallback{
            override fun onSuccess(response: String, code: Int) {
                infoMessage.setTextColor(Color.GREEN)
                summonerFound = true
                infoMessage.text = "Summoner $summonerName is found. Do you want to process?\nIf not, search again for another summoner!"
                summonerModel = Gson().fromJson(response, SummonerModel::class.java)
                summonerModel.summonerRegion = region
            }

            override fun onError(response: String, code: Int) {
                infoMessage.setTextColor(Color.RED)
                summonerFound = false
                infoMessage.text = getString(R.string.summoner_isn_t_found_try_again)
            }
        })
    }
}
