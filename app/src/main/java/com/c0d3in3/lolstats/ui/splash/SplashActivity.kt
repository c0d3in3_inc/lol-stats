package com.c0d3in3.lolstats.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.c0d3in3.lolstats.R
import com.c0d3in3.lolstats.api.ApiHandler
import com.c0d3in3.lolstats.base.syncData
import com.c0d3in3.lolstats.model.SummonerModel
import com.c0d3in3.lolstats.ui.dashboard.DashboardActivity
import com.c0d3in3.lolstats.ui.sign.SignActivity
import com.c0d3in3.lolstats.user.UserInfo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.*

class SplashActivity : AppCompatActivity() {

    private var dataLoaded = false
    private val auth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        init()
    }

    private val runnable = Runnable{
        stopSplash()
    }

    private fun init() {

        if(auth.currentUser != null){
            UserInfo.uid = auth.currentUser!!.uid
            UserInfo.email = auth.currentUser!!.email.toString()
            UserInfo.username = auth.currentUser!!.displayName.toString()
            getUserData()
        }
    }

    private fun getUserData(){
        val db = Firebase.firestore
        db.collection("users").document(auth.currentUser!!.displayName.toString()).get().addOnSuccessListener {
            val summonerModel = it.toObject(SummonerModel::class.java)
            if (summonerModel != null) {
                ApiHandler.createClient(summonerModel.summonerRegion)
                syncData(summonerModel)
                dataLoaded = true
                stopSplash()
            }
            else stopSplash()
        }
    }

    override fun onResume() {
        if(dataLoaded) stopSplash()
        if(auth.currentUser == null) Handler().postDelayed(runnable, 3000)
        super.onResume()
    }

    override fun onPause() {
        Handler().removeCallbacks(runnable)
        super.onPause()
    }


    private fun stopSplash(){
        if(dataLoaded){
            val intent = Intent(this, DashboardActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
        else{
            val intent = Intent(this, SignActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
    }
}
