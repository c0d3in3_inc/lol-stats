package com.c0d3in3.lolstats.user

import com.google.firebase.auth.FirebaseAuth

object UserInfo {
    var uid =  ""
    var email = ""
    var username = ""
    var summonerId = ""
    var accountId = ""
    var summonerUsername = ""
    var summonerLevel : Long = 0
    var summonerIconId = 0
    var summonerRegion = ""
    var favoriteChampion : Long = -1
    var rank = ""
}