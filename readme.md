
# About League of Legends

League of Legends (LoL) is a multiplayer online battle arena video game developed and published by Riot Games for Microsoft Windows and macOS. In League of Legends, players assume the role of a "champion" with unique abilities and battle against a team of other player- or computer-controlled champions. The goal is usually to destroy the opposing team's "Nexus", a structure that lies at the heart of a base protected by defensive structures, although other distinct game modes exist as well with varying objectives, rules, and maps. Each League of Legends match is discrete, with all champions starting off relatively weak but increasing in strength by accumulating items and experience over the course of the game. Champions span a variety of roles and blend a variety of fantasy tropes, such as sword and sorcery, steampunk, and Lovecraftian horror. Although the discrete nature of each match prohibits an overarching narrative in-game, the various champions make up a large and ever-evolving fictional universe developed by Riot Games through short stories, comics, cinematics, and books

## აპლიკაციის მიზანი
აპლიკაცია შეიქმნა League of Legends-ის მოთამაშეებისთვის, რათა მათ შესაძლებლობა ჰქონდეთ გაანალიზონ მათი/სხვა მოთამაშეების performance მატჩებში.

### გამოყენება
აპლიკაცია ჩართვისთანავე მოითხოვს ავტორიზაცია/რეგისტრაციას. ექაუნთის შექმნის შემდგომ აპლიკაცია სთავაზობს მომხმარებელს მოძებნოს თავისი ექაუნთი და მისი ექაუნთის გარშემო მოაწყოს აპლიკაცია  
**ავტორიზაცია ** https://i.imgur.com/bVoTjbt.jpg  
** რეგისტრაცია ** https://i.imgur.com/K7NK50N.jpg  
** სამონერის არჩევა წარმატებით ** https://i.imgur.com/dKSPPXd.jpg  
** სამონერის არჩევა წარუმატებლად  ** https://i.imgur.com/ZTjAjyW.jpg  
** Home გვერდი ** https://i.imgur.com/fyH7kz9.jpg  
** search გვერდი ** https://i.imgur.com/QTuINtI.jpg | https://i.imgur.com/vQr6lDM.jpg  
** profile გვერდი ** https://i.imgur.com/j2Kyz2k.jpg  
** summoner-ის შეცვლის გვერდი ** https://i.imgur.com/ewGtt1D.jpg  

აპლიკაცია ასევე ინახავს თქვენს მეილს თუ მონიშნავ remember me-ს checkbox-ს.  

## Used API 
LoL stats დაფუძნებულია Riot API-ზე. all rights are reserved by Riot Games.

## Used Libraries
LoL stats uses following libraries:  
**Retrofit 2**"https://square.github.io/retrofit/"  აპი-სთვის
**CardView**  დიზაინი
**Material** დიზაინი
**Room** [under develop XD]
**Coroutines** [json, api-სთან სამუშაოდ]
**Swipe Refresh Layout** [მონაცემების გასანაახლებლად]
**Gson** "https://github.com/google/gson"  [json-ის გასაპარსად]
**Glide** "https://github.com/bumptech/glide"  [სურათებისთვის]
**Firebase Auth** "firebase.google.com"   [ავტორიზაცია/რეგისტრაცია]
**Firebase Firestore** "firebase.google.com"  [მომხმარებლის ინფოს შესანახად]

**LoL Stats** was created under **Riot Games' "Legal Jibber Jabber"** policy using assets owned by **Riot Games.  Riot Games** does not endorse or sponsor this project.**